# Cabinet d'assurance

Un cabinet d’assurances soucieux d'améliorer sa productivité décide des'informatiser. Ses gestionnaires, à l'avant-garde du progrès ont décidé de vous
embaucher pour réaliser ce changement. Vous abordez ainsi votre premier jour de travail en commençant par l'analyse des besoins et la modélisation des données du
cabinet.

Le schéma de la base de données que vous allez définir est issu de l'interprétation du cahier des charges dont voici le texte simplifié :

L’ activité principale du cabinet d’assurances consiste à servir d’intermédiaire entre des compagnies d’assurances et des clients qui ont souscrit un contrat auprès de ces compagnies. Plus précisément, l’essentiel du temps des différents agents du cabinet est occupé par les tâches suivantes :
- Ouverture de nouveaux contrats auprès des clients
 (Outre la prospection de nouveaux clients, cela consiste à négocier le contenu du contrat avec le client, établir le contrat en partant de l’un des contrats types de la compagnie d’assurances choisie par le client, et le faire approuver par la compagnie). Il y a souvent des aller-retour dans cette procédure. Un client peut avoir plusieurs contrats d’assurances, mais ils doivent tous être souscrits auprès de la même compagnie. De même, un agent s’occupe de tous les contrats d’un client.
- Encaissement annuel des primes d’assurances souscrits par ses clients, en reversant à chacune des compagnies ce qui lui revient.
- Traitement des sinistres. L’agent est chargé d’évaluer le montant de l’indemnité que la compagnie versera au client pour un sinistre. Après avoir fait son évaluation, il la transmet à la compagnie qui règle directement le client. En ce qui concerne la réparation des préjudices causés par un sinistre (qu’il s’agisse de la réparation d’un bien immobilier ou d’une voiture, du rachat d’objets volés ou détériorés, ou encore de soins médicaux), le client s’en charge tout seul, il utilise comme il le veut le montant de l’indemnité versée par la compagnie.

Pour simplifier, on suppose qu’un sinistre comporte un seul préjudice et qui ne sont pas tous évalués de la même façon.
Pour établir le montant de l’indemnité, l’agent procède différemment selon les cas :
- soit il fait lui-même une évaluation à priori du montant des réparations, il établit alors un rapport d’expertise qu’il adresse à la compagnie, et celle-ci le
transmet au client ;
- soit enfin l’agent d’assurance calcule l’indemnité à partir de factures présentées par le client ; 

L’indemnité est égale à l’intégralité du montant de la facture dans certains cas (c’est notamment celui des frais médicaux) ; dans d’autres cas, on
considère que le bien était déjà partiellement amorti au moment du sinistre, et le montant de l’indemnité ne couvre qu’une partie de celui de la facture.